

from django.shortcuts import render,redirect
from django.db.models import Q
from django.http import HttpResponse
from django.views.generic import CreateView,ListView,DeleteView,UpdateView,DetailView
#from haystack.forms import SearchForm
from django.contrib import messages
from django.db.models import Avg,Count
import io
from .models import ModuleGroup
from .models import student
from .models import grade
from .models import module_has_student
from .models import Module
from urllib import request
from django.urls import reverse_lazy
import csv
#from django.utils.datastructures import MultiValueDictKeyError
# Create your views here.
def index(request):   
    return render(request,'index.html')
# Import data

def import_student(request):
    if request.method == "POST":
        csv_file = request.FILES["csv_file"]
        if not csv_file.name.endswith('.csv'):
            messages.error(request, 'File is not CSV type')
            return redirect("import_data")
        # reading csv file
        data_set = csv_file.read().decode('UTF-8')
        io_string = io.StringIO(data_set)
        next(io_string) # skip the header
        for column in csv.reader(io_string, delimiter=';', quotechar="|"):
            _, created = student.objects.update_or_create(
                matricule=column[0],
                defaults={
                    'name': column[1],
                    'surname': column[2],
                    'date_of_birth' : column[3],
                    'departement': column[4],                  
                }
            )
        return redirect("index")
    return render(request, "student_import.html")
def import_modulegroup(request):
    if request.method == "POST":
        csv_file = request.FILES["csv_file"]
        if not csv_file.name.endswith('.csv'):
            messages.error(request, 'File is not CSV type')
            return redirect("import_data")
        # reading csv file
        data_set = csv_file.read().decode('UTF-8')
        io_string = io.StringIO(data_set)
        next(io_string) # skip the header
        for column in csv.reader(io_string, delimiter=';', quotechar="|"):
            _, created = ModuleGroup.objects.update_or_create(
                code=column[0],
                defaults={
                    'label': column[1],
                    'semestre': column[2],
                    'departement' : column[3],           
                }
            )
        return redirect("index")
    return render(request, "modulegroup_import.html")
def import_module(request):
    if request.method == "POST":
        csv_file = request.FILES["csv_file"]
        if not csv_file.name.endswith('.csv'):
            messages.error(request, 'File is not CSV type')
            return redirect("import_data")
        # reading csv file
        data_set = csv_file.read().decode('UTF-8')
        io_string = io.StringIO(data_set)
        next(io_string) # skip the header
        for column in csv.reader(io_string, delimiter=';', quotechar="|"):
            _, created = Module.objects.update_or_create(
                code=column[0],
                defaults={
                    'label': column[1],
                    'coef': column[2],
                    'modulegroupecode' : column[3],                  
                }
            )
        return redirect("index")
    return render(request, "module_import.html")
def import_module_has_student(request):
    if request.method == "POST":
        csv_file = request.FILES["csv_file"]
        if not csv_file.name.endswith('.csv'):
            messages.error(request, 'File is not CSV type')
            return redirect("import_data")
        # reading csv file
        data_set = csv_file.read().decode('UTF-8')
        io_string = io.StringIO(data_set)
        next(io_string) # skip the header
        for column in csv.reader(io_string, delimiter=';', quotechar="|"):
            _, created = module_has_student.objects.update_or_create(
                module_code=column[0],
                defaults={
                    'student_id': column[1],
                    'year': column[2],
                }
            )
        return redirect("index")
    return render(request, "module_has_student_import.html")
def import_grade(request):
    if request.method == "POST":
        csv_file = request.FILES["csv_file"]
        if not csv_file.name.endswith('.csv'):
            messages.error(request, 'File is not CSV type')
            return redirect("import_data")
        # reading csv file
        data_set = csv_file.read().decode('UTF-8')
        io_string = io.StringIO(data_set)
        next(io_string) # skip the header
        for column in csv.reader(io_string, delimiter=';', quotechar="|"):
            _, created = grade.objects.update_or_create(
                identifiant=column[0],
                defaults={
                    'note_sn': column[1],
                    'note_sc': column[2],
                    'year' : column[3],
                    'module_code': column[4], 
                    'student_id' :column[5],
                    'modulegroup':column[6]            
                }
            )
        return redirect("index")
    return render(request, "grade_import.html")



def home(request):
    departments = ModuleGroup.objects.values_list('departement', flat=True).distinct()
    semesters = ModuleGroup.objects.values_list('semestre', flat=True).distinct()
    if request.method == 'POST':
        departement = request.POST.get('departement')
        semestre = request.POST.get('semestre')
        
        module_grades = grade.objects.filter(modulegroup__departement=departement,
                                         modulegroup__semestre=semestre)
        modules_data = module_grades.values('module_code').annotate(average_grade=Avg('note_sn')).distinct()
        print(modules_data)
        
        labels=[]
        data=[]
        for module in modules_data:
              labels.append(module['module_code'])
              data.append(module['average_grade'])
        
        print(labels)
        print(data)
        return render(request, 'home_page.html', {'labels': labels, 'data': data})
    else:
        return render(request, 'home_page.html', {'departments': departments, 'semesters': semesters})









#CRUD Views

def search_modulegroup_list(request):
    if request.method == "POST":
        module_groupe_code = request.POST['module_groupe_code'].strip()
        print(module_groupe_code)
        modulegroup_list = ModuleGroup.objects.filter(Q(code=module_groupe_code) | Q(label=module_groupe_code))
        print(modulegroup_list)
        return render(request, 'sid21016/modulegroup_list.html',locals())
    else :
        return render(request, 'sid21016/modulegroup_list.html',locals()) 

def search_module_list(request):
    if request.method == "POST":
        module_code = request.POST['module_code'].strip()
        print(module_code)
        module_list = Module.objects.filter(Q(code=module_code) | Q(label=module_code))
        print(module_list)
        return render(request, 'sid21016/module_list.html',locals())
    else :
        return render(request, 'sid21016/module_list.html',locals()) 


def search_student_list(request):
    if request.method == "POST":
        student_matricule = request.POST['matricule'].strip()
        print(student_matricule)
        student_list = student.objects.filter(Q(matricule=student_matricule))
        print(student_list)
        return render(request, 'sid21016/student_list.html',locals())
    else :
        return render(request, 'sid21016/student_list.html',locals()) 


def search_modulehasStudent_list(request):
    if request.method == "POST":
        year1 = request.POST['year']
        print(year1)
        modulehasStudent_list = module_has_student.objects.filter(Q(year=year1))
        print(modulehasStudent_list)
        return render(request, 'sid21016/module_has_student_list.html',locals())
    else :
        return render(request, 'sid21016/module_has_student_list.html',locals()) 

def search_grade_list(request):
    if request.method == "POST":
        grade_identifiant  = request.POST['identifiant']
        print(grade_identifiant)
        grade_list = grade.objects.filter(Q(identifiant=grade_identifiant))
        print(grade_list)
        return render(request, 'sid21016/grade_list.html',locals())
    else :
        return render(request, 'sid21016/grade_list.html',locals()) 


class ModuleGroupCreate(CreateView):
    model = ModuleGroup
    fields = '__all__'

class StudentCreate(CreateView):
    model= student
    fields = '__all__'
class GradeCreate(CreateView):
    model= grade
    fields = '__all__'
class ModuleHasStudentCreate(CreateView):
    model= module_has_student
    fields = '__all__'
class ModuleCreate(CreateView):
    model= Module
    fields = '__all__'

class ModuleGroupList(ListView):
    print(ModuleGroup)
    model = ModuleGroup
class StudentList(ListView):
    model= student
    
class GradeList(ListView):
    model= grade
    
class ModulehasStudentList(ListView):
    model= module_has_student
    
class ModuleList(ListView):
    model= Module
    
class ModuleGroupDelete(DeleteView):
    model = ModuleGroup
    success_url = reverse_lazy('index')
class ModuleGroupUpdate(UpdateView):
    model = ModuleGroup
    fields = '__all__'
class ModuleGroupDetail(DetailView):
    model = ModuleGroup

class ModuleDelete(DeleteView):
    model = Module
    success_url = reverse_lazy('index')
class ModuleUpdate(UpdateView):
    model = Module
    fields = '__all__'
class ModuleDetail(DetailView):
    model = Module
class StudentDelete(DeleteView):
    model = student
    success_url = reverse_lazy('index')
class StudentUpdate(UpdateView):
    model = student
    fields = '__all__'
class StudentDetail(DetailView):
    model = student
class ModulehasStudentDelete(DeleteView):
    model = module_has_student
    success_url = reverse_lazy('index')
class ModulehasStudentUpdate(UpdateView):
    model = module_has_student
    fields = '__all__'
class ModulehasStudentDetail(DetailView):
    model = module_has_student
class GradeDelete(DeleteView):
    model = grade
    success_url = reverse_lazy('index')
class GradeUpdate(UpdateView):
    model = grade
    fields = '__all__'
class GradeDetail(DetailView):
    model = grade




