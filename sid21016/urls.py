from django.urls import path
from . import views
from .views import import_student,home,import_grade,import_module,import_module_has_student,import_modulegroup

urlpatterns= [ 
    path('grade/add',views.GradeCreate.as_view(),name="grade-form"),
    path('grade_import/', import_grade, name='data-grade'),
    path('module_import/', import_module, name='data-module'),
    path('module_has_student_import/', import_module_has_student, name='data-module_has_student'),
    path('module_group_import/', import_modulegroup, name='data-module_group'),
    path('student_import/', import_student, name='data-student'),
    path('',views.index , name = 'index'),
    path('home-page/', home, name='home-page'),
    path('modulehasstudent/search', views.search_modulehasStudent_list, name= 'modulehasStudent-search' ),
    path('grade/search', views.search_grade_list , name= 'grade-search' ),
    path('module/search', views.search_module_list , name= 'module-search' ),
    path('modulegroup/search', views.search_modulegroup_list , name= 'modulegroup-search' ),
    path('student/search', views.search_student_list , name= 'student-search' ),

    path('modulegroup/add',views.ModuleGroupCreate.as_view(),name="modulegroup-create"),
    path('modulegroup/list',views.ModuleGroupList.as_view(),name="modulegroup-list"),
    path('modulegroup/delete/<str:pk>', views.ModuleGroupDelete.as_view() , name= 'modulegroup-delete' ),
    path('modulegroup/update/<str:pk>', views.ModuleGroupUpdate.as_view() , name= 'modulegroup-update' ),
    path('modulegroup/<str:pk>', views.ModuleGroupDetail.as_view() , name= 'modulegroup-detail' ),
     
    path('student/add',views.StudentCreate.as_view(),name="student-form"),
    path('student/list',views.StudentList.as_view(),name="student-list"),
    path('student/delete/<int:pk>', views.  StudentDelete.as_view() , name= 'student-delete' ),
    path('student/update/<int:pk>', views.StudentUpdate.as_view() , name= 'student-update' ),
    path('student/<int:pk>', views.StudentDetail.as_view() , name= 'student-detail' ),

    path('module/add',views.ModuleCreate.as_view(),name="module-form"),
    path('module/list',views.ModuleList.as_view(),name="module-list"),
    path('module/delete/<str:pk>', views.ModuleDelete.as_view() , name= 'module-delete' ),
    path('module/update/<str:pk>', views.ModuleUpdate.as_view() , name= 'module-update' ),
    path('module/<str:pk>', views.ModuleDetail.as_view() , name= 'module-detail' ),

    path('grade/add',views.GradeCreate.as_view(),name="grade-form"),
    path('grade/list',views.GradeList.as_view(),name="grade-list"),
    path('grade/delete/<int:pk>', views.GradeDelete.as_view() , name= 'grade-delete' ),
    path('grade/update/<int:pk>', views.GradeUpdate.as_view() , name= 'grade-update' ),
    path('grade/<int:pk>', views.GradeDetail.as_view() , name= 'grade-detail' ),

    path('modulehasstudent/add',views.ModuleHasStudentCreate.as_view(),name="modulehasstudent-form"),
    path('modulehasstudent/list',views.ModulehasStudentList.as_view(),name="modulehasstudent-list"),
    path('modulehasstudent/delete/<int:pk>', views.ModulehasStudentDelete.as_view() , name= 'modulehasstudent-delete' ),
    path('modulehasstudent/update/<int:pk>', views.ModulehasStudentUpdate.as_view() , name= 'modulehasstudent-update' ),
    path('modulehasstudent/<int:pk>', views.ModulehasStudentDetail.as_view() , name= 'modulehasstudent-detail' ),
    
    
    
    
    


    
]

