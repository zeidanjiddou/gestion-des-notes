from django.db import models
from django.urls import reverse
from django.core.validators import MinValueValidator, MaxValueValidator
# Create your models here.
class ModuleGroup(models.Model):
    code = models.CharField(max_length=10,unique=True,primary_key=True)
    label = models.CharField(max_length=255)
    semestre = models.CharField(max_length=255,default='S2')
    departement = models.CharField(max_length=255,default='SID')
    def __str__(self):
        return self.code + ' - ' + self.label
    # Methods
    def get_absolute_url(self): 
        return reverse('modulegroup-list')
    
class Module(models.Model):
    code = models.CharField(default=0,max_length=10,unique=True,primary_key=True)
    label = models.CharField(max_length=255)
    coef = models.IntegerField()
    modulegroupecode = models.ForeignKey(ModuleGroup,on_delete=models.SET_NULL,blank=False,null=True)
    def __str__(self):
        return self.code + ' - ' + self.label
    # Methods
    def get_absolute_url(self):
        return reverse('module-list')
    
class student(models.Model):
    matricule = models.IntegerField(unique=True,primary_key=True)
    name = models.CharField(max_length=45)
    surname = models.CharField(max_length=45)
    date_of_birth = models.DateField()
    departement = models.CharField(max_length=45)
    # Methods
    def __int__(self):
        return self.matricule 
    def get_absolute_url(self):
        return reverse('student-list')
    
class module_has_student(models.Model):
    module_code=models.ForeignKey(Module,on_delete=models.SET_NULL,null=True)
    student_id=models.ForeignKey(student,on_delete=models.SET_NULL,null=True)
    year=models.IntegerField(unique=True,primary_key=True)
    class Meta:
        unique_together = ('module_code', 'student_id', 'year')

    # Methods
    def get_absolute_url(self):
        return reverse('index')        

class grade(models.Model):
    identifiant = models.IntegerField(unique=True,primary_key=True)
    note_sn = models.FloatField(validators=[MinValueValidator(0)])
    note_sc = models.FloatField(validators=[MaxValueValidator(20)])
    year = models.PositiveIntegerField(validators=[MinValueValidator(2000)])
    module_code = models.ForeignKey(Module, related_name="code_1",on_delete=models.SET_NULL,null=True)
    student_id = models.ForeignKey(student, related_name="ide",on_delete=models.SET_NULL,null=True)
    modulegroup = models.ForeignKey(ModuleGroup, on_delete=models.CASCADE)
    def get_absolute_url(self):
        return reverse('index')
