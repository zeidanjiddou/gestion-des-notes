from django.contrib import admin
from .models import ModuleGroup
from .models import student
from .models import grade
from .models import module_has_student
from .models import Module
# Register your models here.
class ModuleGroupAdmin(admin.ModelAdmin):
    pass
class ModuleAdmin(admin.ModelAdmin):
    pass
admin.site.register(ModuleGroup,ModuleGroupAdmin)
admin.site.register(Module,ModuleAdmin)